<div class="row">
	<div class="col-xs-12">
		<?php if (isset($this->params['search'])) : ?>
			<div class="page-header">
				<h3>
					Search results for <b><?php echo $this->output($this->params['search']); ?></b>
				</h3>
			</div>
		<?php endif; ?>

		<?php if (empty($this->params['contacts']) && !empty($this->params['search'])) : ?>
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1>No contacts found!</h1>
					<p>
						Add one by clicking the "Add a contact" button
					</p>

				</div>
			</div>
		<?php endif; ?>
		<?php if (empty($this->params['contacts']) && empty($this->params['search'])) : ?>
			<div class="row">
				<div class="col-xs-12 text-center">
					<h1>No contacts yet!</h1>
					<p>
						Add one by clicking the "Add a contact" button
					</p>
				</div>
			</div>
		<?php endif; ?>
		<?php foreach ($this->params['contacts'] as $contact) : ?>
			<div class="contact-row" id="contact_row_<?php echo $this->output($contact["id"]); ?>">
				<div class="row" style="border-bottom: 1px solid #0D86BA; margin-bottom: 10px;">
					<div class="col-xs-12"><h3><?php echo $this->output($contact['name']); ?> <?php echo $this->output($contact['surname']); ?></h3></div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<span class="fa fa-envelope fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['email']); ?>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<span class="fa fa-mobile fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['cellphone_nr']); ?>
					</div>
					<div  class="col-xs-12 col-sm-4 col-md-4">
						<span class="fa fa-phone fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['telephone_nr']); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4">
						<span class="fa fa-print fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['fax_nr']); ?>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<span class="fa fa-home fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['physical_address']); ?>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4">
						<span class="fa fa-envelope fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['postal_address']); ?>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<span class="fa fa-info fa-lg fa-fw" aria-hidden="true"></span>&nbsp;
						<?php echo $this->output($contact['additional_information']); ?>
					</div>
				</div>
				<div class="trash-action">
					<span id="contact_trash_<?php echo $this->output($contact["id"]); ?>" class="glyphicon glyphicon-trash trash-button" aria-hidden="true"></span>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>