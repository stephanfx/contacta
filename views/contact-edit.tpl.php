<form action="/contact/update" id="update_contact_form" method="POST" class="form-horizontal" role="form">
<?php $contact = $this->contact; ?>
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Edit Contact</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label for="inputName" class="col-sm-4 control-label">Name<span class="required">*</span>:</label>
                      <div class="col-sm-8">
                        <input type="text" name="name" id="inputName" class="form-control" value="<?php echo $this->output($contact['name']); ?>" required="required" pattern="[a-zA-Z0-9\s]+" title="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputSurname" class="col-sm-4 control-label">Surname<span class="required">*</span>:</label>
                      <div class="col-sm-8">
                        <input type="text" name="surname" id="inputSurname" class="form-control" value="<?php echo $this->output($contact['surname']); ?>" required="required" pattern="[a-zA-Z0-9\s]+" title="">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputEmail" class="col-sm-4 control-label">Email<span class="required">*</span>:</label>
                      <div class="col-sm-8">
                        <input type="email" name="email" id="inputEmail" class="form-control" value="<?php echo $this->output($contact['email']); ?>" required="required" title="">                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputCellphone_nr" class="col-sm-4 control-label">Cellphone Nr<span class="required">*</span>:</label>
                      <div class="col-sm-8">
                        <input type="text" name="cellphone_nr" id="inputCellphone_nr" class="form-control" value="<?php echo $this->output($contact['cellphone_nr']); ?>" pattern="^\+27[0-9]{9}$" required="required">
                        <p class="help-block">format: +27123456789</p>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputTelephone_nr" class="col-sm-4 control-label">Telephone Nr:</label>
                      <div class="col-sm-8">
                        <input type="text" name="telephone_nr" id="inputTelephone_nr" class="form-control" value="<?php echo $this->output($contact['telephone_nr']); ?>" pattern="^\+27[0-9]{9}$">
                        <p class="help-block">format: +27123456789</p>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputFax_nr" class="col-sm-4 control-label">Fax Nr:</label>
                      <div class="col-sm-8">
                        <input type="text" name="fax_nr" id="inputFax_nr" class="form-control" value="<?php echo $this->output($contact['fax_nr']); ?>" pattern="^\+27[0-9]{9}$">
                        <p class="help-block">format: +27123456789</p>
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputPostal_address" class="col-sm-4 control-label">Postal Address:</label>
                      <div class="col-sm-8">
                        <input type="text" name="postal_address" id="inputPostal_address" class="form-control" value="<?php echo $this->output($contact['postal_address']); ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputPhysical_address" class="col-sm-4 control-label">Physical Address:</label>
                      <div class="col-sm-8">
                        <input type="text" name="physical_address" id="inputPhysical_address" class="form-control" value="<?php echo $this->output($contact['physical_address']); ?>">
                      </div>
                    </div>

                    <div class="form-group">
                      <label for="inputAdditional_information" class="col-sm-4 control-label">Additional Information:</label>
                      <div class="col-sm-8">
                        <input type="text" name="additional_information" id="inputAdditional_information" class="form-control" value="<?php echo $this->output($contact['additional_information']); ?>">
                      </div>
                    </div>
                    <input type="hidden" name="id" value="<?php echo $this->output($contact['id']); ?>">
                    <input type="hidden" name="date_created" value="<?php echo $this->output($contact['date_created']); ?>">
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="xsrf" value="<?php echo $this->xsrf_token; ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save contact</button>
                </div>
            </div>
        </div>
</form>