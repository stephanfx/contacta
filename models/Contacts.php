<?php

namespace Contacta\Models;

use Contacta\Lib\Model;
/**
* Contacts class
*/
class Contacts extends Model
{

	protected $table = 'contacts';

	public function searchContacts($search)
	{
		$columns = $this->getColumns();
		$params = array();
		$where = array();
		foreach ($columns as $column) {
			$where[] = $column['Field'] . " LIKE ?";
			$params[] = "%" . $search . "%";
		}
		$whereString = " WHERE (" . implode (" OR ", $where) . ")";
		$whereString .= " AND date_deleted IS NULL ";
		return $this->fetchAll($whereString, $params);
	}

	public function validate(array $params)
	{
		$valid = true;
		$columns = $this->getColumns();


		foreach ($columns as $column) {
			// dont check these fields
			if ($column['Field'] == 'id' OR $column['Field'] == 'date_created'){
				continue;
			}
			// quick check to see if the column is required, throw exception if it is
			if (
				(!array_key_exists($column['Field'], $params) && $column['Null'] == 'NO' )
				OR
				($column['Null'] == 'NO' && empty($params[$column['Field']]))
			){
				throw new \PDOException("'" . $column['Field'] . "' is a required field.");
			}
		}
		return $valid;
	}

}