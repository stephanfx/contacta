<?php

namespace Contacta\Lib;

/**
* Model class to be extended by the model
*/
class Model
{

	protected $connection = null;
	protected $columns = [];
	protected $primary = null;


	function __construct()
	{
		if ($this->connection === null){
			$this->connection = Db::getInstance();
			$this->getColumns();
		}

	}

	public function find($id)
	{
		$columns = $this->getColumns();
		foreach ($columns as $column) {
			$columnString[] = $column['Field'];
			if ($column['Key'] === "PRI") {
				$primary = $column['Field'];
			}
		}
		$query = sprintf("SELECT %s FROM %s WHERE %s = ?", implode(', ', $columnString), $this->table, $primary);

		$statement = $this->executeStatement($query, array($id));
		return $statement->fetch();
	}

	public function fetchAll($where = '', array $params = array(), $orderBy = "")
	{
		// make column string to select actually just the values from the table
		$columns = $this->getColumns();
		foreach ($columns as $column) {
			$columnString[] = $column['Field'];
		}
		$query = sprintf("SELECT %s FROM %s", implode(', ', $columnString), $this->table);
		$query .= $where;
		$query .= $orderBy;

		$statement = $this->executeStatement($query, $params);
		$results = $statement->fetchAll();
		return $results;
	}

	public function getColumns()
	{
		if (empty($this->columns)){
			$query = sprintf("DESCRIBE %s", $this->table);
			$statement = $this->connection->prepare($query);
			$statement->execute();
			$this->columns = $statement->fetchAll();
			// set the primary column for further use
			foreach ($this->columns as $column) {
				if ($column['Key'] === "PRI") {
					$this->primary = $column['Field'];
				}
			}
		}
		return $this->columns;
	}

	/**
	 * Generic Insert to be used on inserts into the tables.
	 * @param  array $params The parameters to be inserted
	 * @return string $result the inserted row
	 */
	public function insert(array $params)
	{
		$columns = $this->getColumns();

		// set the id and date created as it not being there will throw an error
		$params['id'] = null;
		$params['date_created'] = date("Y-m-d H:i:s", time());

		// step through the table columns and match data in the parameters passed through
		foreach ($columns as $column) {
			if (array_key_exists($column['Field'], $params)) {
				$columnArr[] = $column['Field'];
				$paramArr[] = $params[$column['Field']];
				$bindArr[] = "?";
			}
		}

		// build query string
		$query = sprintf(
			"INSERT INTO %s (%s) VALUES (%s)",
			$this->table,
			implode(', ', $columnArr),
			implode(', ', $bindArr)
		);
		$this->executeStatement($query, $paramArr);
	}

	public function update(array $params, $delete = false)
	{
		$columns = $this->getColumns();

		// set the modified date
		$params['date_modified'] = date("Y-m-d H:i:s", time());

		foreach ($columns as $column) {
			// no need to set the date_created and the id of the record
			if ($column['Field'] == 'date_created' OR $column['Field'] == 'id'){
				continue;
			}
			if (array_key_exists($column['Field'], $params)) {
				$columnArr[] = $column['Field'] . " = ?";
				$paramArr[] = $params[$column['Field']];
			}
			// quick check to see if the column is required, throw exception if it is
			if (!array_key_exists($column['Field'], $params) && $column['Null'] == 'NO'){
				throw new \Exception($column['Field'] . " is a required field");
			}
		}
		// add the primary identifier
		$paramArr[] = $params[$this->primary];

        $query = sprintf(
        	"UPDATE %s SET %s WHERE %s = ?",
        	$this->table,
        	implode(', ', $columnArr),
        	$this->primary
        );
        $statement = $this->executeStatement($query, $paramArr);
        return $statement->fetch();
    }

    public function delete($id)
    {
    	$date_deleted = date("Y-m-d H:i:s", time());
    	$query = sprintf(
    		"UPDATE %s SET date_deleted = ? WHERE %s = ?",
    		$this->table,
    		$this->primary
    	);
    	$statement = $this->executeStatement($query, array($date_deleted, $id));
    	return $statement->fetch();
    }

	/**
	 * Executes a query 'safely' and throws an error if there is an error along the
	 * way.
	 *
	 * @param  string 		$query  	The query to executen
	 * @param  array 		$params 	The parameters to be passed to the execute method
	 * @return PDOStatement $statement 	The statement that was executed.
	 */
	public function executeStatement($query, array $params)
	{
		$statement = $this->connection->prepare($query);
		$statement->execute($params);

		if ($statement->errorCode() !== "00000"){
			$error = $statement->errorInfo();
			throw new \PDOException($error[0] . " - " . $error[2]);
		}
		return $statement;
	}

}