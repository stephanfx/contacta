<?php

namespace Contacta\Lib;

/**
* DB Singleton Class
*/
class Db
{

	private static $db = null;

	private function __construct()
	{
		# code...
	}


	/**
	 * Gets a connection instance for use in the application
	 * @return self::$db Database connection
	 */
	public static function getInstance()
	{
		if (!isset(self::$db)){
			$user = 'root';
			$pass = '123';
			try {
				// get the config
				require FILE_ROOT . '/config.php';
				extract($config);
				// make a persistent connection
				$pdo_attr = array(

    				\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION
				);
				// connect to the db
			    self::$db = new \PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
			} catch (\PDOException $e) {
				// cath the error, output and stop execution
			    print "Error!: " . $e->getMessage() . "<br/>";
			    die();
			}
		}
		return self::$db;
	}


}