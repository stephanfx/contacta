<?php

namespace Contacta\Lib;


/**
* Controller class to be extended from controllers classes
*/
class Controller
{
	public $params = [];
	public $xsrf_token = null;

	function __construct($params = array())
	{
		if (!empty($params)){
			$this->params = $params;
		}
	}

	public function loadView($name)
	{
		include FILE_ROOT . '/views/' . $name;
	}

	public function output($param = "")
	{
		if (!empty($param)) {
			return filter_var($param, FILTER_SANITIZE_STRING);
		} else {
			return false;
		}
	}


	public function setXSRFToken()
	{
		$_SESSION['XSRF'] = uniqid();
		$this->xsrf_token = $_SESSION['XSRF'];
	}

	public function checkXSRF(array $params)
	{
		if (!array_key_exists('xsrf', $params)){
			throw new \Exception('No valid token passed along.');
		}

		if ($params['xsrf'] !== $_SESSION['XSRF']){
			throw new \Exception('Tokens do not match.');
		}
	}
}