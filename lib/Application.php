<?php

namespace Contacta\Lib;

/**
* Application Class to handle routing, requests and responses
*
*/
class Application
{

	protected $params = [];
	protected $controller = 'Contacta\\Controllers\\Index';
	protected $method = 'index';
	protected $uri = "";
	protected $route = [];

	public function run()
	{
		try {
			// get all request variables
			$this->params = $_REQUEST;

			$this->uri = parse_url($_SERVER['REQUEST_URI']);
			// parse request uri
			$this->route = explode('/', filter_var($this->uri['path'], FILTER_SANITIZE_URL));
			// no route specified, go to default
			if ($this->uri['path'] !== "/"){
				$this->controller = 'Contacta\\Controllers\\' . ucfirst($this->route[1]);
				$this->method = $this->route[2];
			}
			$this->controller = new $this->controller($this->params);
			call_user_func_array(array($this->controller, $this->method), $this->params);
		} catch (\Exception $e) {
			http_response_code(500);
			echo "SERVER ERROR : " . $e->getMessage();
		} catch (\PDOException $pe){
			http_response_code(500);
			echo "DATABASE ERROR : " . $pe->getMessage();
		}
	}
}