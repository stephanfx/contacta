Contacta
========

Simple Contact Manager
----------------------

### Installation
* Extract contents of zip to folder on server
* Setup a virtual host to use the extracted folder with any domain you wish
* Add host entries if required
* Restore the DB file in the 'db' folder
* Open the 'config.php' file and update the db details
* navigate to configured virtual host

### Tested on
* Windows 8.1
* Chrome (latest)

### Data
* Generated from http://www.generatedata.com/