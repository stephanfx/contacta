<?php

namespace Contacta\Controllers;

use Contacta\Lib\Controller;
use Contacta\Models\Contacts;

/**
* Contact Class to handle all crud operations
*/
class Contact extends Controller
{

	public function index()
	{
		// load all data
		$contacts = new Contacts();
		if (isset($this->params['search'])){
			$this->params['contacts'] = $contacts->searchContacts($this->params['search']);
		} else {
			$this->params['contacts'] = $contacts->fetchAll(" WHERE date_deleted IS NULL ", array(), " ORDER BY name");
		}

		$this->loadView("contact-list.tpl.php");

	}

	public function edit()
	{
		// get the id
		$id = (int) $this->params['id'];
		$contacts = new Contacts();
		$contact = $contacts->find($id);
		$this->contact = $contact;
		$this->setXSRFToken();
		$this->loadView('contact-edit.tpl.php');
	}

	public function update()
	{
		// check if the XSRF token is set and valid
		$this->checkXSRF($this->params);

		// get new contacts model
		$contacts = new Contacts();

		// validate form parameters passed through.
		$contacts->validate($this->params);

		// save the record
		$contact = $contacts->update($this->params);

		return json_encode($contact);
	}

	public function add()
	{
		$this->setXSRFToken();
		$this->loadView("contact-add.tpl.php");
	}

	public function store()
	{
		// check the XSRF token is set and valid
		$this->checkXSRF($this->params);

		// get contact model
		$contacts = new Contacts();

		//validate fields are valid
		$contacts->validate($this->params);

		// save the record.
		$contact = $contacts->insert($this->params);
		return json_encode($contact);
	}

	public function delete()
	{
		$contacts = new Contacts();
		$id = $this->params['id'];
		$contact = $contacts->delete($id);
	}

}