<?php

namespace Contacta\Controllers;

use Contacta\Lib\Controller;
/**
* Index Controller
*
* This is the 'home' controller
*/
class Index extends Controller
{

	public function index($params = '')
	{
		$this->loadView("index.tpl.html");
	}
}