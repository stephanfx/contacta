<?php


define('FILE_ROOT', dirname(__FILE__));

session_start();
/**
 * ErrorException handler to let application class catch and pass errors;
 * @param  [type] $severity [description]
 * @param  [type] $message  [description]
 * @param  [type] $file     [description]
 * @param  [type] $line     [description]
 * @return [type]           [description]
 */
function exception_error_handler($severity, $message, $file, $line) {
    if (!(error_reporting() & $severity)) {
        // This error code is not included in error_reporting
        return;
    }
    throw new ErrorException($message, 0, $severity, $file, $line);
}
set_error_handler("exception_error_handler");

// Autoloader implementation
spl_autoload_register(function($class){

	$prefixes = array(
		"Contacta\\Lib" => FILE_ROOT . '/lib',
		"Contacta\\Controllers" => FILE_ROOT . '/controllers',
		"Contacta\\Models" => FILE_ROOT . '/models'
		);

	foreach ($prefixes as $name => $dir) {
		$len = strlen($name);
		if (strncmp($name, $class, $len) !== 0){
			continue;
		} else {
			$relative_class = substr($class, $len);

			$file = $dir . str_replace("\\" , '/', $relative_class) . '.php';
			if (file_exists($file)){
				require $file;
				break;
			}
		}
	}
});

$application = new \Contacta\Lib\Application();
$application->run();