(function() {
    'use strict';
    $(document).ready(function() {

        getList();

        $('#search_text').on('input', function(e) {
            e.preventDefault();
            var search = $(this).val();
            if (search.length < 2) {
                $('#clear_search').hide();
                getList();
                return false;
            }
            var params = {
                search: $(this).val()
            };
            $('#clear_search').show();
            getList(params);
        });

        $('#clear_search').on('click', function(e){
            e.preventDefault();
            $('#search_text').val('');
            $(this).hide();
            getList();
        })


        $('body').on('click', '.contact-row', function(e) {
            e.preventDefault();
            var id = $(this).attr('id').replace('contact_row_', '');
            loadEditForm(id);
        });

        /**
         * Click event on the trash can to display the confirmation dialog
         */
        $('body').on('click', '.contact-row .trash-button', function(e) {
            // e.preventDefault();
            e.stopPropagation();
            var id = $(this).attr('id').replace('contact_trash_', '');
            showDeleteConfirmation(id);
        });

        // load the add contact form
        $('.add-contact').on('click', function(e) {
            loadAddForm();
        });

        /**
         * Submit a new contact to the backend
         *
         */
        $('#myModal').on('submit', '#add_contact_form', function(e) {
            e.preventDefault();
            var formVar = $(this).serialize();

            $.post('/contact/store', formVar, function(data) {
                $('#myModal').modal('hide');
                addAlert('.alert-space', 'success', 'You have successfully created a new contact.', 5000);
                // TODO: hide spinner
            }).fail(function(error) {
                addAlert('#myModal .modal-body', 'danger', error.responseText, 3000);
            }).done(function() {
            	getList();
            });
        });

        /**
         * Submit an update of a contact
         */
        $('#myModal').on('submit', '#update_contact_form', function(e) {
            e.preventDefault();
            var formVar = $(this).serialize();

            // send data to backend
            $.post('/contact/update', formVar, function(data) {
            	getList();
                $('#myModal').modal('hide');
                addAlert('.alert-space', 'success', 'You have successfully update the contact.', 5000);
            }).fail(function(error) {
                addAlert('#myModal .modal-body', 'danger', error.responseText, 3000);
            }).done(function(){

            });
        });

        /**
         * Unbind the bound functions on the modals as they stack
         */
        $('#myConfirmModal').on('hidden.bs.modal', function(e) {
            $('#myConfirmModal .btn').off('click');
        });
    });

    /**
     * Get the contact list and loads it in
     * @param  {object} params The params to be sent to the backend
     * @return {void}
     */
    function getList(params) {
        if (typeof(params) == "undefined") {
            params = $('#search_text').val();
        }
        // load contact list
        $('.contact-list').load('/contact/index', params, function(response, status, xhr) {
        	if (xhr.status == 500){
	        	addAlert('.alert-space', 'danger', response);
        	}
        });
    }

    /**
     * Loads the form for adding a contact and puts it in the body of the modal
     * @return {void}
     */
    function loadAddForm() {
        $('#myModal').load('/contact/add', {}, function(html) {
            $('#myModal').modal('show');
        });
    }

    /**
     * Loads the edit form editing a contact
     * @param  {int} 	id 	The ID of the contact
     * @return {void}
     */
    function loadEditForm(id) {
        $('#myModal').load('/contact/edit', {
            "id": id
        }, function(html) {
            $('#myModal').modal('show');
        });
    }

    /**
     * Display the confirmation trash dialog
     * @param  {int} 	id 	The id to delete
     * @return {void}
     */
    function showDeleteConfirmation(id) {
        $('#myConfirmModal').modal('show');
        $('#myConfirmModal .btn').on('click', function(e) {
            var answer = $(this).attr('id');
            if (answer == "confirm_yes") {
                $.post('/contact/delete', {
                    "id": id
                }, function() {
                    // TODO: Success alert and hide spinner
                    $('#myConfirmModal').modal('hide');
                    addAlert('.alert-space', 'success', 'You have successfully deleted the contact.', 5000);
                    getList();
                }).fail(function(error) {
                    addAlert('#myConfirmModal .modal-body', 'danger', error.responseText, 3000);

                });
            }
        });
    }

    /**
     * Adds an alert in a specific places and removes it after certain amount of time.
     * @param {string} 	location selectors
     * @param {string} 	type     style of alert
     * @param {string} 	text     text to display in alert
     * @param {int} 	delay    time in milliseconds to show alert
     */
    function addAlert(location, type, text, delay) {
        var alertBox = '<div class="alert alert-' + type;
        alertBox += '" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">';
        alertBox += '<span aria-hidden="true">&times;</span></button>' + text + '</div>';

        $(location).prepend(alertBox);
        $('.alert').alert();

        if (typeof(delay) !== "undefined"){
	        // set timeout to remove alert
	        setTimeout(function() {
	            $('.alert').alert('close');
	        }, delay);
        }
    }
})();
